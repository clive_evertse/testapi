<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFiledsToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            
            $table->renameColumn('name', 'fullname');
            $table->string('mobile')->after('email');
            $table->string('wechat_id')->after('mobile')->nullable();
            $table->string('engena_token')->after('wechat_id')->nullable();
            $table->string('strava_token')->after('engena_token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
