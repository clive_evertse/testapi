<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::any('register',['as' => 'engena.register', 'uses' => 'EngenaController@register']);
Route::any('getUserProfile',['as' => 'engena.getUserProfile', 'uses' => 'EngenaController@getUserProfile']);
Route::any('updateProfile',['as' => 'engena.updateProfile', 'uses' => 'EngenaController@updateProfile']);
Route::any('profile',['as' => 'engena.profile', 'uses' => 'EngenaController@profile']);
Route::any('search_trails',['as' => 'engena.search_trails', 'uses' => 'EngenaController@search_trails']);
Route::any('displayTrailInfo/{name}',['as' => 'engena.displayTrailInfo', 'uses' => 'EngenaController@displayTrailInfo']);
Route::any('thankyou',['as' => 'engena.thankyou', 'uses' => 'EngenaController@thankyou']);
Route::any('thankyou_redirect',['as' => 'engena.thankyou_redirect', 'uses' => 'EngenaController@thankyou_redirect']);
Route::any('loginUser',['as' => 'engena.loginUser', 'uses' => 'EngenaController@loginUser']);
Route::any('registerUser',['as' => 'engena.registerUser', 'uses' => 'EngenaController@registerUser']);
Auth::routes();

Route::get('/home', 'HomeController@index');

Route::any('logout', function(){
		Auth::logout();
});
