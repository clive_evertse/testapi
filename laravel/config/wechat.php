<?php
return [
    //=== WeChat API settings ===
    /**
     * These values can be set in the .env file.  Defaults are listed.
     * 
     * = WeChat Authentication values =
     * WECHAT_TOKEN=null
     * WECHAT_APPID=null
     * WECHAT_SECRET=null
     * 
     * = WeChat payment details =
     * WECHAT_PAYMENT_MERCHANTID=null
     * WECHAT_PAYMENT_MERCHANTKEY=null
     * WECHAT_PAYMENT_PAYSIGN=null (optional)
     * 
     * = Location of config file for menu/events =
     * WECHAT_CONFIG_FILE=resources/wechat.json  
     * 
     * = Enable debug, if enabled some error messages will be sent back on the OA =
     * WECHAT_DEBUG=false
     * 
     * = Limit users that can access the service. Looks at allowed_users array. =
     * WECHAT_LIMIT_USERS=false
     * WECHAT_ALLOWED_USERS=null  (use wechat_hash, example: oarCPuNO_4LSvDbHZT_hT_ryMZQo)
     * 
     */
    
    //=== Payment API settings ===
    //Set the interface to the relevant controller
    'payment-interface' => 'App\Http\WeChat\PaymentController',
    
    //Route prefix for built-in wechat routes.  e.g. 'wechat' results in http://domain.com/wechat/ being used.
    'route' => 'wechat',
    
];
