@extends('layouts.app')
@section('title', 'Thank You')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Registration Successfull</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/thankyou_redirect') }}">
                        {{ csrf_field() }}
                        <div class="col-md-8 col-md-offset-2">
                            <p>Thank you {{$username}}</p>
                            <p>You have successfully created your Engena Account.</p>
                            <p>Your Strava account can also be linked</p>
                            <p>You can now enjoy all the functionalities of our Official Account.</p>
                            </br>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">
                                    Done
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
