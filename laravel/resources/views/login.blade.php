<!DOCTYPE html>
	<html>
		<head>
			<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
			<title>Register</title>
		</head>
		<body>
		 	<div class="container">
        		<div class="row">
            		<div class="col-md-10 col-md-offset-1">               
						<div class="content">
							<form class="form-horizontal" action="{{url('login_user')}}" method="POST">
								{{ csrf_field() }}
								@if (count($errors) > 0)
								    <div class="container">
								        <div class="row">
								            <div class="col-md-8 col-md-offset-1">
								               <ul>
								                   @foreach ($errors->all() as $error)
								                        <div class="alert alert-warning" role="alert">{{$error}}</div>
								                    @endforeach
								                </ul>
								            </div>
								        </div>
								    </div>
								@endif
								@if(isset($message))
									
										<div class="alert alert-warning">
		  									{{$message}}
										</div>
									
								@endif
				  				<div class="form-group">
									<input type="text" class="form-control" id="username" aria-describedby="emailHelp" placeholder="Username" name="username">   
  								</div>
  								<div class="form-group">
    								<input type="password" class="form-control" id="password" placeholder="Password" name="password">
  								</div>
  								<button type="submit" class="btn btn-primary">Log In</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</body>
	</html>