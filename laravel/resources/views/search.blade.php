
@extends('layouts.app')
@section('title', 'Search for a Trail')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Search For A Trail</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/displayTrailInfo') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">
                        		<select id="region" class="form-control">
    								<option>Please Select One</option>
    								@for ($i = 0; $i < count($regions['data']); $i++)
    									<option val="<?php echo str_replace(' ', '_',  $regions['data'][$i]['name']); ?>">{{$regions['data'][$i]['name']}}</option>
    								<?php	$regions['data'][$i]['name'] = str_replace(' ', '_',  $regions['data'][$i]['name']); ?>    								
    								@endfor
								</select>

                        </div>
                        </div>
                        <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">

								<select id="reserves" class="form-control">
								
								</select>
                        </div>
                        </div>
                         <div class="form-group">
                        <div class="col-md-8 col-md-offset-2">

								<select id="trails" class="form-control">
								</select>
                        </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-primary btn-block" id="submit">
                                    Done
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
<script type="text/javascript">

@for($i =0;$i <count($reserves); $i++)
var  {{$regions['data'][$i]['name']}} = new Array();
	@for($j=0;$j < count($reserves[$regions['data'][$i]['name']]);$j++)
		  {{$regions['data'][$i]['name']}}[{{$j}}] = "{{$reserves[$regions['data'][$i]['name']][$j]}}";
	@endfor
@endfor


@for($i =0;$i <count($trails); $i++)
<?php $keys = array_keys($trails);?>
var  {{$keys[$i]}} = new Array();
	@for($j=0;$j < count($trails[$keys[$i]]);$j++)
	<?php $trail_keys = array_keys($trails[$keys[$i]]);?>
		  {{$keys[$i]}}[{{$j}}] = "{{$trails[$keys[$i]][$trail_keys[$j]]}}";
	@endfor
@endfor

populateReserve();


$("#submit").click(function(){
	alert($('#trails').val());
	name = $('#trails').val();
	alert(name);
	$.ajax({
		type: "POST",
		    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    	url: "http://engena-wechat.clivee.56.dev/displayTrailInfo/"+decodeURIComponent(name),
    	success: function(){
      		
    	}
	});	
	return false;
});


$(function() {

      $('#region').change(function(){
      	
        populateReserve();
    });

      $('#reserves').change(function(){
      	populateTrail();
      });

    
});


function populateReserve(){
    region=$('#region').val();
    $('#reserves').html('');
    $('#trails').html('');
    
    
    if(region=='Stellenbosch'){
    	$('#reserves').append('<option>Please Select One</option>');
        Stellenbosch.forEach(function(t) { 
            $('#reserves').append('<option>'+t+'</option>');
        });
    }
    
    if(region=='Somerset West'){
    	$('#reserves').append('<option>Please Select One</option>');
        Somerset_West.forEach(function(t) {
            $('#reserves').append('<option name="'+t+'">'+t+'</option>');
        });
    }
    
}


function populateTrail(){
    
    reserve=$('#reserves').val();
    $('#trails').html('');
    
    
    if(reserve=='Jonkershoek'){
    	$('#trails').append('<option>Please Select One</option>');
        Jonkershoek.forEach(function(t) { 
            $('#trails').append('<option>'+t+'</option>');
        });
    }
    
    if(reserve=='Helderberg Farm'){
    	$('#trails').append('<option>Please Select One</option>');
        Helderberg_Farm.forEach(function(t) {
            $('#trails').append('<option>'+t+'</option>');
        });
    }

    if(reserve=='True Grit Bike Park'){
    	$('#trails').append('<option>Please Select One</option>');
        True_Grit_Bike_Park.forEach(function(t) {
            $('#trails').append('<option>'+t+'</option>');
        });
    }

    if(reserve=='Stellenbosch Open Trails by STF'){
    	$('#trails').append('<option>Please Select One</option>');
        Stellenbosch_Open_Trails_by_STF.forEach(function(t) {
            $('#trails').append('<option>'+t+'</option>');
        });
    }
    
}  
</script>
@endsection
