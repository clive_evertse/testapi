@extends('layouts.app')
@section('title', 'Home')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                   "Thank you for signing up! An email has been sent to you to confirm your email address and activate your account."
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
