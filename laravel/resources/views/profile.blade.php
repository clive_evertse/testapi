@extends('layouts.app')
@section('title', 'Profile')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Profile</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/updateProfile') }}">
                        {{ csrf_field() }}
                        @if(isset($messages))
							@foreach($messages as $message)
								<div class="alert alert-warning">
		  							{{$message}}
								</div>
							@endforeach
						@endif
  					<div class="form-group">
            <div class="col-md-8 col-md-offset-2">
						<input type="text" class="form-control" id="fullname" aria-describedby="emailHelp" placeholder="{{$user_info['fullName']}}" name="fullname">   
  					</div>
            </div>
    				<div class="form-group">
            <div class="col-md-8 col-md-offset-2">
						<input type="text" class="form-control" id="mobile" aria-describedby="emailHelp" placeholder="{{$user_info['mobile']}}" name="mobile">    
  					</div>
            </div>
  					<div class="form-group">
            <div class="col-md-8 col-md-offset-2">
						<input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="{{$user_info['email']}}" name="email">   
  					</div>
            </div>
            <div class="form-group">
            <div class="col-md-8 col-md-offset-2">
  					<button type="submit" class="btn btn-success btn-block">Link My Strava Account</button> </br></br>
  					<button type="submit" class="btn btn-primary btn-block">Register</button>
            </div>
				</form>
			</div>
		</body>
	</html>