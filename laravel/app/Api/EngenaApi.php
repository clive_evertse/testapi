<?php

namespace App\Api;

use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Config;
use Log;
use URL;

class EngenaApi{

	function __construct(){

	}


    public function registerUser($username,$password,$fullname,$email,$mobile,$wechat_id){
        $url = "https://engena.co.za/api/auth/register";
        $object = [
            "username" => $username,
            "password" => $password,
            "fullname" => $fullname,
            "email" => $email,
            "mobile" => $mobile
        ];

        $object = json_encode($object);

        $response = $this->post($url,$object,$wechat_id);
        return json_decode($response);
    }

    public function loginUser($username,$password,$wechat_id){
        $url = "https://engena.co.za/api/auth/login";
        $object = [
            "username" => $username,
            "password" => $password
        ];

        $object = json_encode($object);

        $response = $this->post($url,$object,$wechat_id);
        return json_decode($response);
    }

    public function getUserData($wechat_id){
        $url = 'https://engena.co.za/api/user';
        $response = $this->get($url,$wechat_id);
        return json_decode($response);
    }

    public function updateUserProfile($wechat_id,$fullname,$mobile,$email){
        $url = 'https://engena.co.za/api/user';
        $object = [
            "fullname" => $fullname,
            "mobile" => $mobile,
            "email" => $email
        ];
        $object = json_encode($object);

        $response = $this->put($url,$wechat_id,$object);
        return json_decode($response);
    }

    public function getRegions($wechat_id){
        $url = 'https://engena.co.za/api/regions';
        $response = $this->get($url,$wechat_id);
        return json_decode($response);
    }

    public function getReserves($wechat_id,$id){
        $url = 'https://engena.co.za/api/reserves?region='.$id;
        $response = $this->get($url,$wechat_id);
        return json_decode($response);
    }

    public function getTrails($wechat_id,$id){
        $url = 'https://engena.co.za/api/trails?reserves='.$id;
        $response = $this->get($url,$wechat_id);
        return json_decode($response);
    }

    public function getTrailInfo($wechat_id,$name){
        $url = 'https://engena.co.za/api/trails?name='.$name;
        $response = $this->get($url,$wechat_id,$name);
        return json_decode($response);
    }    
    public function post($url, $object,$wechat_id) {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $object);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','WeChat-ID: '.$wechat_id,'Content-Length: ' . strlen($object)));

        $response = curl_exec($ch);

        curl_close($ch);
        
        return $response;     
    }

    public function get($url,$wechat_id,$name=null) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','WeChat-ID: '.$wechat_id));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $response = curl_exec($ch); 
        curl_close($ch);
        
        return $response;     
    }

    public function put($url,$wechat_id,$object){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",            
            CURLOPT_POSTFIELDS => $object,
            CURLOPT_HTTPHEADER => array(
                'WeChat-ID: '.$wechat_id,
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        return $response; 
    }
}