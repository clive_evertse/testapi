<?php

namespace App\Api;

use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Config;
use Log;
use URL;

class EngenaAPI{

	function __construct(){

	}


    public function registerUser($username,$password,$fullname,$email,$mobile){
        $url = "https://engena.co.za/api/auth/register";
        $object = [
            "username" => $username,
            "password" => $password,
            "fullname" => $fullname,
            "email" => $email,
            "mobile" => $mobile
        ];

        $object = json_encode($object);

        $response = $this->post($url,$object);
        return json_decode($response);
    }

    public function loginUser($username,$password){
        $url = "https://engena.co.za/api/auth/login";
        $object = [
            "username" => $username,
            "password" => $password
        ];

        $object = json_encode($object);

        $response = $this->post($url,$object);
        return json_decode($response);
    }

    public function post($url, $object) {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $object);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($object)));
        $response = curl_exec($ch);

        curl_close($ch);
        
        return $response;     
    }

    public function get($url) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $response = curl_exec($ch); 
        curl_close($ch);
        
        return $response;     
    }
}