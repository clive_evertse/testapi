<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Api\EngenaApi;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

        /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function login(Request $request)
    {
        $wechat_id = $request->get('userid');
        $data = Input::all();
        $logingEngena = new EngenaApi();
        //$wechat_id = "o6_bmjrPTlm6_2sgVt7hMZOPfL2M";
        $response = $logingEngena->loginUser($data['username'],$data['password'],$wechat_id);
        $response = json_decode(json_encode($response), true);
        
        if(isset($response['token'])){
            $user_info = $logingEngena->getUserData($wechat_id);
            $user_info = json_decode(json_encode($user_info),true);
            echo '<pre>';
            print_r($user_info);
            echo '</pre>';
        }
        elseif($response['status_code'] == 401 || $response['status_code'] == 422){
          $message = $response['message'];
          return view('auth/login',array('message' => $message));
        } 
    }


 }
