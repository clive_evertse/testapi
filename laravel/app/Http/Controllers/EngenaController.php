<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Log;
use URL;
use Validator;
use View;
use Config;
use Cache;
use Methys\WeChatLaravel\WeChatController;
use App\Api\EngenaApi;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class EngenaController extends Controller
{


    public function registerUser(Request $request){
      $wechat_id = $request->get('userid');
      $apiTest = new EngenaApi();
      $data = Input::all();
      //$wechat_id = "o6_bmjrPTlm6_2sgVt7hMZOPfL2M";
      $response = $apiTest->registerUser($data['username'],$data['password'],$data['fullname'],$data['email'],$data['mobile'],$wechat);
      $response = json_decode(json_encode($response), true);
      if(isset($response['errors'])){
        
        if(isset($response['errors']['username'])){
          $messages[] = $response['errors']['username'][0];
        }
        if(isset($response['errors']['mobile'])){
          $messages[]  = $response['errors']['mobile'][0];
        }
        if(isset($response['errors']['email'])){
          $messages[] = $response['errors']['email'][0];
        }

        return view('register',array('messages' => $messages));
      }
      elseif(isset($response['status'])){
          $messages[] = "Please verify your email to validate your account";
          return view('thankyou',array('username' => $data['username']));
      }
    }

    public function getUserProfile(Request $request){
      $wechat_id = $request->get('userid');
      $apiTest = new EngenaApi();
      $wechat_id = "o6_bmjrPTlm6_2sgVt7hMZOPfL2M";

      $response = $apiTest->getUserData($wechat_id);      
      return $response;
    }

    public function profile(Request $request){
        $wechat_id = $request->get('userid');
        $user_info = $this->getUserProfile();
        $user_info = json_decode(json_encode($user_info),true);

        return view('profile',array('user_info' => $user_info ));
    }

    public function updateProfile(Request $request){
      $wechat_id = $request->get('userid');
      $data = Input::all();
      //print_r($data );die();
      $apiTest = new EngenaApi();
      $wechat_id = "o6_bmjrPTlm6_2sgVt7hMZOPfL2M";
      $fullname = $data['fullname'];
      $mobile = $data['mobile'];
      $email = $data['email'];

      $response = $apiTest->updateUserProfile($token,$fullname,$mobile,$email);
      $response = json_decode(json_encode($response), true);

      if(isset($response['errors'])){
        
        if(isset($response['errors']['username'])){
          $messages[] = $response['errors']['username'][0];
        }
        if(isset($response['errors']['mobile'])){
          $messages[]  = $response['errors']['mobile'][0];
        }
        if(isset($response['errors']['email'])){
          $messages[] = $response['errors']['email'][0];
        }

        $user_info = $this->getUserProfile();
        $user_info = json_decode(json_encode($user_info),true);

        return view('profile',array('messages' => $messages, 'user_info' => $user_info ));
      }
      elseif(isset($response['status'])){
          echo "Profile Updated";
      }
    }

    public function search_trails(Request $request){
      $wechat_id = $request->get('userid');
      $engenaApi = new EngenaApi();

      $wechat_id = "o6_bmjrPTlm6_2sgVt7hMZOPfL2M";

      $regions = $engenaApi->getRegions($wechat_id);
      $regions = json_decode(json_encode($regions), true);

      for ($i=0; $i < count($regions); $i++) { 
        for ($j=0; $j < count($regions['data'][$i]); $j++) { 
            $reserves[$j] = $engenaApi->getReserves($wechat_id,$regions['data'][$j]['id']);
            $reserves = json_decode(json_encode($reserves), true);
            
        }
      }

      for ($i=0; $i < count($reserves); $i++) { 
        for ($j=0; $j < count($reserves[$i]['data']); $j++) { 
            $trails[$j] = $engenaApi->getTrails($wechat_id,$reserves[$i]['data'][$j]['id']);
            $trails = json_decode(json_encode($trails), true);
            $reserve_array[str_replace(' ', '_',  $reserves[$i]['data'][$j]['region']['name'])][$j] = $reserves[$i]['data'][$j]['name'];
        }
      }

      for ($i=0; $i < count($trails); $i++) { 
        for ($j=0; $j < count($trails[$i]['data']); $j++) {
            $trails_array[str_replace(' ', '_',  $trails[$i]['data'][$j]['reserve']['name'])][$j] = $trails[$i]['data'][$j]['name'];
        }
      }
      return view('search', array('regions' => $regions, 'reserves' => $reserve_array, 'trails' => $trails_array));
    }

    public function displayTrailInfo($name){

        $data = $name;
 
        $engenaApi = new EngenaApi();   
        $wechat_id = "o6_bmjrPTlm6_2sgVt7hMZOPfL2M";
        $trailInfo = $engenaApi->getTrailInfo($wechat_id,$name);
        return view('home');
    }

    public function register(Request $request){
      $wechat_id = $request->get('userid');
      return view('register');
    }

   public function thankyou(){
      $username = "Clive";
      return view('thankyou',array('username' => $username));
    }
    
    public function thankyou_redirect(){
      
      echo "RMC will be sent to user";
    }
}