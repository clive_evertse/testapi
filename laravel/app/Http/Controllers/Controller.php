<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Methys\WeChat\ConfigController;
use Methys\WeChat\Messages\Packet;
use Methys\WeChat\WeChat;
use Methys\WeChatLaravel\Handlers\CacheHandler;
use Methys\WeChatLaravel\Handlers\EventHandler;
use Methys\WeChatLaravel\Handlers\UrlHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
